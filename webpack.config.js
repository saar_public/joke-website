const path = require('path')
const HtmlWebpackPlugin = require("html-webpack-plugin")
module.exports = {
    mode: "production",
    // mode: "development",
    entry: {
        bundel: path.resolve(__dirname, "src/index.js"),
    },
    output: {
        path: path.resolve(__dirname, "public"),
        filename: '[name][contenthash].js',
        clean: true,
        assetModuleFilename: "[name][ext]"
    },
    devServer: {
        static: {
            directory: path.resolve(__dirname, "public")
        },
        port: 8080,
        open: true,
        hot: true,
        compress: true,
        historyApiFallback: true,
    },
    devtool: "source-map",
    plugins: [
        new HtmlWebpackPlugin({
            title: "JOCKS",
            filename: "index.html",
            template: "src/template.html",
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader",
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset/resource"
            },
        ]
    }
}