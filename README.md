step 1
git clone https://gitlab.com/saar_public/joke-website.git
step 2
npm install
step 3
npm run build

for deploy on cloudflare pages
./node_modules/wrangler/bin/wrangler.js pages publish ./dist
OR
npm run cloudflarePageDeploy

for deploy on firebase hosting
./node_modules/firebase-tools/lib/bin/firebase.js login
./node_modules/firebase-tools/lib/bin/firebase.js init hosting
./node_modules/firebase-tools/lib/bin/firebase.js deploy --only hosting
